import { defineConfig } from "vite";
import { default as path, resolve } from "node:path";
import { fileURLToPath } from "node:url";

import istanbul from "rollup-plugin-istanbul";
import dts from "vite-plugin-dts";

const dirname = path.dirname(fileURLToPath(import.meta.url));

// https://vitejs.dev/config/

/** @type {import('vite').UserConfig} */
export default defineConfig(({ mode }) => {
  const config = {
    plugins: [ dts({ rollupTypes: true }) ],
    build: {
      lib: {
        entry: resolve(dirname, "src/index.js"),
        formats: [ "es", "cjs" ]
      },
      sourcemap: true
    }
  };

  if (mode === "test" || process.env.COVERAGE === "1") {
    config.plugins.push(istanbul({ include: [ "src/**/*.js" ] }));
  }
  return config;
});
