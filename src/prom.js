// @ts-check
// SPDX-License-Identifier: Zlib
// SPDX-FileCopyrightText: 2024 CERN (home.cern)
// SPDX-Created: 2024-02-19T10:01:22
// SPDX-FileContributor: Author: Sylvain Fargier <sylvain.fargier@cern.ch>

/**
 * @template T
 * @brief Deferred object class, regrouping resolve/reject and promise
 */
export class Deferred {
  /** @type {Promise<T>} */
  promise;

  isPending = false;

  /** @type {(() => any)|null} */
  abort = null;

  /** @type {(value: T) => void} */
  resolve = (_val) => undefined;

  /** @type {(err: Error|any) => void} */
  reject = (_err) => undefined;

  constructor() {
    this.promise = new Promise((resolve, reject) => {
      this.isPending = true;
      this.resolve = (val) => {
        this.isPending = false;
        resolve(val);
      };
      this.reject = (err) => {
        this.isPending = false;
        reject(err);
      };
    });
  }
}

/**
 * @template T=any
 * @return {Deferred<T>}
 */
export function makeDeferred() {
  return new Deferred();
}

/**
 * @brief adds a timeout on deferred
 * @template T=any
 * @param {Deferred<T>} deferred
 * @param {number} ms
 * @param {(Error|any)=} error
 * @return {Promise<T>}
 */
export function deferredTimeout(deferred, ms, error) {
  /** @type {NodeJS.Timeout|null} */
  var timeoutId = setTimeout(function() {
    if (!error || typeof error === "string") {
      error = new Error(error || "Timed out after " + ms + " ms");
      error.code = "ETIMEDOUT";
    }
    timeoutId = null;
    const rej = deferred.reject;
    deferred.resolve = function() {};
    deferred.reject = function() {};
    rej(error);
    if (deferred.abort) {
      deferred.abort();
    }
  }, ms);

  return deferred.promise.finally(() => {
    if (timeoutId) { clearTimeout(timeoutId); }
  });
}

/**
 * @brief adds a timeout on promise
 * @template T=any
 * @param {Promise<T>} prom
 * @param {number} ms
 * @param {(Error|any)=} error
 * @return {Promise<T>}
 */
export function promiseTimeout(prom, ms, error) {
  return new Promise(function(resolve, reject) {
    let resolved = false;
    const timeoutId = setTimeout(function() {
      if (!error || typeof error === "string") {
        error = new Error(error || "Timed out after " + ms + " ms");
        error.code = "ETIMEDOUT";
      }
      resolved = true;
      reject(error);
    }, ms);

    prom.then(function(value) {
      if (!resolved) {
        clearTimeout(timeoutId);
        resolve(value);
      }
    }, function(error) {
      if (!resolved) {
        clearTimeout(timeoutId);
        reject(error);
      }
    });
  });
}

/**
 * @brief adds a timeout on promise
 * @template T=any
 * @param {Deferred<T>|Promise<T>} promise
 * @param {number} ms
 * @param {(Error|any)=} error
 * @return {Promise<T>}
 */
export function timeout(promise, ms, error) {
  if (promise && "resolve" in promise) {
    return deferredTimeout(/** @type {Deferred<T>} */ (promise),
      ms, error);
  }
  else {
    return promiseTimeout(/** @type {Promise<T>} */ (promise), ms, error);
  }
}

/**
 * @template T=undefined
 * @param {number} ms
 * @param {T} [ret]
 * @return {Promise<T>}
 */
export function delay(ms, ret) {
  return new Promise(function(resolve) {
    setTimeout(function() { resolve(/** @type {T} */ (ret)); }, ms);
  });
}

/**
 * @template T=any
 * @param  {(ret: T) => any} fun
 * @return {(ret: T) => Promise<T>}
 */
export function tap(fun) {
  return function(/** @type {T} */ ret) {
    return Promise.resolve(fun(ret))
    .then(() => ret);
  };
}
