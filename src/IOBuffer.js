/* eslint-disable max-lines */
// SPDX-License-Identifier: Zlib
// SPDX-FileCopyrightText: 2024 Sylvain Fargier
// SPDX-Created: 2024-03-23T16:41:45
// SPDX-FileContributor: Author: Sylvain Fargier <fargier.sylvain@gmail.com>

import { VarBuffer } from "./VarBuffer.js";

/** @type {TextEncoder} */
let encoder;
/** @type {TextDecoder} */
let decoder;

/**
 * @typedef { Int8Array | Uint8Array | Uint8ClampedArray | Int16Array |
 *   Uint16Array | Int32Array | Uint32Array | Float32Array | Float64Array |
 *   BigInt64Array | BigUint64Array} TypedArray
 */
export class IOBufferIn {
  fail = true;

  /** @type {Uint8Array|null} */
  buffer = null;

  /** @type {DataView|null} */
  #view = null;

  #idx = 0;

  /**
   * @param {VarBuffer|ArrayBuffer|TypedArray|null|undefined} buffer
   */
  constructor(buffer) {
    this.setBuffer(buffer);
  }

  /**
   * @param {VarBuffer|ArrayBuffer|TypedArray|null|undefined} buffer
   */
  // eslint-disable-next-line complexity
  setBuffer(buffer) {
    if (buffer instanceof Uint8Array) {
      this.buffer = buffer;
    }
    else if (buffer instanceof ArrayBuffer) {
      this.buffer = new Uint8Array(buffer);
    }
    else if (buffer?.buffer instanceof ArrayBuffer) {
      this.buffer = new Uint8Array(buffer.buffer,
        /** @type {TypedArray} */ (buffer)?.byteOffset ?? 0,
        /** @type {TypedArray} */ (buffer)?.byteLength ?? 0);
    }
    else if (buffer instanceof VarBuffer && buffer.buffer.buffer) {
      this.buffer = buffer.buffer;
      this.#view = new DataView(buffer.buffer.buffer,
        buffer.buffer?.byteOffset ?? 0,
        buffer.buffer?.byteLength ?? 0);
    }

    this.fail = this.buffer ? false : true;
    this.#view = this.buffer ? new DataView(this.buffer.buffer) : null;
    this.#idx = 0;
  }

  /**
   * @param {number} size
   * @return {boolean}
   */
  #checkSize(size) {
    if (!this.fail && !(this.#idx + size <= (this.buffer?.length ?? NaN))) {
      this.fail = true;
    }
    return !this.fail;
  }

  get bytesLeft() {
    return (this.fail) ? 0 : ((this.buffer?.length ?? 0) - this.#idx);
  }

  readUint8(_littleEndian = true) {
    if (!this.#checkSize(1)) { return NaN; }
    return this.#view?.getUint8(this.#idx++) ?? NaN;
  }

  readInt8(_littleEndian = true) {
    if (!this.#checkSize(1)) { return NaN; }
    return this.#view?.getInt8(this.#idx++) ?? NaN;
  }

  readUint16(littleEndian = true) {
    if (!this.#checkSize(2)) { return NaN; }
    const ret = this.#view?.getUint16(this.#idx, littleEndian) ?? NaN;
    this.#idx += 2;
    return ret;
  }

  readInt16(littleEndian = true) {
    if (!this.#checkSize(2)) { return NaN; }
    const ret = this.#view?.getInt16(this.#idx, littleEndian) ?? NaN;
    this.#idx += 2;
    return ret;
  }

  readUint32(littleEndian = true) {
    if (!this.#checkSize(4)) { return NaN; }
    const ret = this.#view?.getUint32(this.#idx, littleEndian) ?? NaN;
    this.#idx += 4;
    return ret;
  }

  readInt32(littleEndian = true) {
    if (!this.#checkSize(4)) { return NaN; }
    const ret = this.#view?.getInt32(this.#idx, littleEndian) ?? NaN;
    this.#idx += 4;
    return ret;
  }

  readBigUint64(littleEndian = true) {
    if (!this.#checkSize(8)) { return NaN; }
    const ret = this.#view?.getBigUint64(this.#idx, littleEndian) ?? NaN;
    this.#idx += 8;
    return ret;
  }

  readBigInt64(littleEndian = true) {
    if (!this.#checkSize(8)) { return NaN; }
    const ret = this.#view?.getBigInt64(this.#idx, littleEndian) ?? NaN;
    this.#idx += 8;
    return ret;
  }

  readFloat(littleEndian = true) {
    if (!this.#checkSize(4)) { return NaN; }
    const ret = this.#view?.getFloat32(this.#idx, littleEndian) ?? NaN;
    this.#idx += 4;
    return ret;
  }

  readDouble(littleEndian = true) {
    if (!this.#checkSize(8)) { return NaN; }
    const ret = this.#view?.getFloat64(this.#idx, littleEndian) ?? NaN;
    this.#idx += 8;
    return ret;
  }

  readFloat64(littleEndian = true) {
    return this.readDouble(littleEndian);
  }

  /**
   * @param  {number} len
   */
  readString(len) {
    if (!this.#checkSize(len)) { return ""; }
    if (!decoder) {
      decoder = new TextDecoder();
    }
    const str = decoder.decode(
      this.buffer?.subarray(this.#idx, this.#idx + len));
    this.#idx += len;
    const idx = str.indexOf("\0");
    return (idx >= 0) ? str.substr(0, idx) : str;
  }

  /**
   * @param  {number} len
   */
  read(len) {
    if (!this.#checkSize(len)) { return null; }

    const ret = this.buffer?.subarray(this.#idx, this.#idx + len);
    this.#idx += len;
    return ret;
  }

  /**
   * @param  {number} len
   */
  fwd(len) {
    if (!this.#checkSize(len)) { return; }
    this.#idx += len;
  }

  rewind() {
    this.setBuffer(this.buffer);
  }

  /**
   * @brief wrap a regular buffer in BufferIn object
   * @param  { VarBuffer |
   *           ArrayBuffer |
   *           TypedArray |
   *           IOBufferIn |
   *           null |
   *           undefined} buffer
   * @return {IOBufferIn}
   */
  static wrap(buffer) {
    if (buffer instanceof IOBufferIn) {
      return buffer;
    }
    return new IOBufferIn(buffer);
  }
}

/**
 * @details endianness defaults to little-endian
 */
export class IOBufferOut {
  fail = true;

  /** @type {Uint8Array|VarBuffer|null} */
  buffer = null;

  /** @type {DataView|null} */
  #view = null;

  #idx = 0;

  /**
   * @param {ArrayBuffer|TypedArray|VarBuffer|null|undefined} buffer
   */
  constructor(buffer) {
    this.setBuffer(buffer);
  }

  /**
   * @param {ArrayBuffer|TypedArray|VarBuffer|null|undefined} buffer
   */
  // eslint-disable-next-line complexity
  setBuffer(buffer) {
    if (buffer instanceof VarBuffer && buffer.buffer.buffer) {
      this.buffer = buffer;
      this.#view = new DataView(buffer.buffer.buffer,
        buffer.buffer?.byteOffset ?? 0,
        buffer.buffer?.byteLength ?? 0);
    }
    else if (buffer instanceof ArrayBuffer) {
      this.buffer = new Uint8Array(buffer);
      this.#view = new DataView(buffer);
    }
    else if (buffer?.buffer instanceof ArrayBuffer) {
      const byteOffset = /** @type {TypedArray} */ (buffer)?.byteOffset ?? 0;
      const byteLength = /** @type {TypedArray} */ (buffer)?.byteLength ?? 0;
      this.buffer = new Uint8Array(buffer.buffer, byteOffset, byteLength);
      this.#view = new DataView(buffer.buffer, byteOffset, byteLength);
    }
    this.fail = this.buffer ? false : true;
    this.#idx = 0;
  }

  get bytesLeft() {
    return (this.fail) ? 0 : ((this.buffer?.length ?? 0) - this.#idx);
  }

  /**
   * @param {number} num
   */
  writeUint8(num, _littleEndian = true) {
    if (!this.#checkSize(1)) { return; }
    this.#view?.setUint8(this.#idx++, num);
  }

  /**
   * @param {number} num
   */
  writeInt8(num, _littleEndian = true) {
    if (!this.#checkSize(1)) { return; }

    this.#view?.setInt8(this.#idx++, num);
  }

  /**
   * @param {number} num
   * @param {boolean} [littleEndian=true]
   */
  writeUint16(num, littleEndian = true) {
    if (!this.#checkSize(2)) { return; }
    this.#view?.setUint16(this.#idx, num, littleEndian);
    this.#idx += 2;
  }

  /**
   * @param {number} num
   * @param {boolean} [littleEndian=true]
   */
  writeInt16(num, littleEndian = true) {
    if (!this.#checkSize(2)) { return; }
    this.#view?.setInt16(this.#idx, num, littleEndian);
    this.#idx += 2;
  }

  /**
   * @param {number} num
   * @param {boolean} [littleEndian=true]
   */
  writeUint32(num, littleEndian = true) {
    if (!this.#checkSize(4)) { return; }
    this.#view?.setUint32(this.#idx, num, littleEndian);
    this.#idx += 4;
  }

  /**
   * @param {number} num
   * @param {boolean} [littleEndian=true]
   */
  writeInt32(num, littleEndian = true) {
    if (!this.#checkSize(4)) { return; }
    this.#view?.setInt32(this.#idx, num, littleEndian);
    this.#idx += 4;
  }

  /**
   * @param {number|bigint} num
   * @param {boolean} [littleEndian=true]
   */
  writeBigUint64(num, littleEndian = true) {
    if (!this.#checkSize(8)) { return; }
    this.#view?.setBigUint64(this.#idx, BigInt(num), littleEndian);
    this.#idx += 8;
  }

  /**
   * @param {number|bigint} num
   * @param {boolean} [littleEndian=true]
   */
  writeBigInt64(num, littleEndian = true) {
    if (!this.#checkSize(8)) { return; }
    this.#view?.setBigInt64(this.#idx, BigInt(num), littleEndian);
    this.#idx += 8;
  }

  /**
   * @param {number} num
   * @param {boolean} [littleEndian=true]
   */
  writeFloat(num, littleEndian = true) {
    if (!this.#checkSize(4)) { return; }
    this.#view?.setFloat32(this.#idx, num, littleEndian);
    this.#idx += 4;
  }

  /**
   * @param {number} num
   * @param {boolean} [littleEndian=true]
   */
  writeDouble(num, littleEndian = true) {
    if (!this.#checkSize(8)) { return; }
    this.#view?.setFloat64(this.#idx, num, littleEndian);
    this.#idx += 8;
  }

  /**
   * @param {string} str
   * @param {number=} len
   */
  writeString(str, len) {
    str = str ?? "";
    len = len || (str.length + 1);
    if (!this.#checkSize(len)) {
      return;
    }
    if (len - 1 < str.length) {
      str = str.slice(0, len - 1);
    }
    str = str.padEnd(len, "\0");
    if (!encoder) {
      encoder = new TextEncoder();
    }
    if (this.buffer instanceof Uint8Array) {
      this.buffer.set(encoder.encode(str), this.#idx);
    }
    else {
      this.buffer?.buffer.set(encoder.encode(str), this.#idx);
    }
    this.#idx += len;
  }

  /**
   * @param {ArrayBuffer|TypedArray|number[]} buffer
   */
  add(buffer) {
    let abuf;
    if (buffer instanceof Uint8Array || buffer instanceof Array) {
      abuf = buffer;
    }
    else if (buffer instanceof ArrayBuffer) {
      abuf = new Uint8Array(buffer);
    }
    else {
      abuf = new Uint8Array(buffer?.buffer);
    }

    if (!this.#checkSize(abuf.length)) { return; }

    if (this.buffer instanceof Uint8Array) {
      this.buffer.set(abuf, this.#idx);
    }
    else {
      this.buffer?.buffer.set(abuf, this.#idx);
    }
    this.#idx += abuf.length;
  }

  /**
   * @param {number} sz
   * @return {boolean}
   */
  #checkSize(sz) {
    if (this.fail) {
      return false;
    }
    else if (!(this.#idx + sz <= (this.buffer?.length ?? NaN))) {
      if (this.buffer instanceof VarBuffer) {
        this.buffer.resize(this.buffer.length + sz);
        this.#view = new DataView(this.buffer.buffer.buffer);

        if (!(this.#idx + sz <= this.buffer.length)) {
          this.fail = true;
          return false;
        }
      }
      else {
        this.fail = true;
        return false;
      }
    }
    return true;
  }

  /**
   * @brief wrap a Buffer or VarBuffer in a BufferOut object
   * @param  { VarBuffer |
   *           ArrayBuffer |
   *           TypedArray |
   *           IOBufferOut |
   *           null |
   *           undefined} buffer
   * @return {IOBufferOut}
   */
  static wrap(buffer) {
    if (buffer instanceof IOBufferOut) {
      return buffer;
    }
    return new IOBufferOut(buffer);
  }
}

