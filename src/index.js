// @ts-check
// SPDX-License-Identifier: Zlib
// SPDX-FileCopyrightText: 2024 CERN (home.cern)
// SPDX-Created: 2024-02-06T14:20:41
// SPDX-FileContributor: Author: Sylvain Fargier <sylvain.fargier@cern.ch>

import * as prom from "./prom.js";
export * from "./prom.js";
export { prom };
export * from "./VarBuffer.js";
export * from "./IOBuffer.js";

/**
 * @brief check if maybe object/array is empty
 * @param {any} value
 * @returns {boolean} true if object is empty or not an enumerable
 */
export function isEmpty(value) {
  return (value?.length ?? value?.size ??
    ((value instanceof Object && Object.entries(value).length) || 0)) === 0;
}

/**
 * @brief test if object is null or undefined
 * @param {any} value
 * @returns {boolean} true if value is nullish
 */
export function isNil(value) {
  return value === undefined || value === null;
}

/**
 * @brief get last element of maybe list
 * @template T
 * @param {T[]|any} value
 * @returns {T|undefined}
 */
export function last(value) {
  const len = value?.length;
  if (isNaN(len)) { return undefined; }

  return value[len - 1];
}

/**
 * @brief get first element of maybe list
 * @template T
 * @param {T[]|any} value
 * @returns {T|undefined}
 */
export function first(value) {
  const len = value?.length; // identify arrays
  if (isNaN(len)) { return undefined; }

  return value[0];
}

/**
 * @brief try to recursively set a value on object
 * @param {any} obj
 * @param {(string|number)[]|any} path
 * @param {any} value
 * @returns {any|null} input object or null on error
 */
export function set(obj, path, value) {
  try {
    let leaf = obj;

    for (let i = 0; i < (path?.length ?? 0) - 1; ++i) {
      if (!(leaf instanceof Object)) { return null; }

      const pathPart = path[i];

      let child = leaf[pathPart];
      if (!(child instanceof Object)) {
        child = {};
        leaf[pathPart] = child;
      }
      leaf = child;
    }
    const key = last(path);
    if (!(leaf instanceof Object) || key === undefined) { return null; }
    leaf[last(path)] = value;
  }
  catch {
    return null;
  }
  return obj;
}

/**
 * @brief deeply clone object (native implementation)
 * @template T
 * @param {T} value
 * @return {T}
 */
export function cloneDeep(value) {
  return structuredClone(value);
}

/**
 * @brief merge sources in target
 * @param {any} target
 * @param  {any[]} sources
 * @returns {any}
 */
export function merge(target, ...sources) {
  if (!(target instanceof Object)) {
    target = {};
  }
  sources.forEach((source) => {
    if (!(source instanceof Object)) { return; }

    Object.entries(source).forEach(([ key, value ]) => {
      const targetValue = target[key];
      if (value === undefined) {
        /* skip as in lodash */
      }
      else if (value instanceof Object && targetValue instanceof Object) {
        merge(targetValue, value);
      }
      else {
        target[key] = cloneDeep(value);
      }
    });
  });

  return target;
}
