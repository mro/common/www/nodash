// @ts-check
// SPDX-License-Identifier: Zlib
// SPDX-FileCopyrightText: 2016 Sylvain Fargier
// SPDX-Created: 2016-11-08T14:04:02+01:00
// SPDX-FileContributor: Author: Sylvain Fargier <fargie_s> <fargier.sylvain@gmail.com>

"use strict";

/** @type {TextEncoder} */
let encoder;
/** @type {TextDecoder} */
let decoder;

/**
 * @brief Variable size buffer
 * @details Variable size buffer for Node and Browser
 */
export class VarBuffer {
  static BUF_SZ = 2048;

  #length = 0;

  /** @type {Uint8Array} */
  #buffer;

  /**
   * @param {?(Uint8Array|number)=} buf
   */
  constructor(buf) {
    if (Number.isInteger(buf)) {
      /* @ts-ignore: checked above */
      this.#buffer = new Uint8Array(buf);
    }
    else if (buf instanceof Uint8Array) {
      this.#buffer = buf;
      this.#length = buf.length;
    }
    else {
      this.#buffer = new Uint8Array(VarBuffer.BUF_SZ);
    }
  }

  reset() {
    this.#length = 0;
  }

  /**
   * @param {Uint8Array|string} data
   */
  add(data) {
    if (typeof data === "string") {
      data = VarBuffer.encode(data).buffer;
    }
    while (data.length > this.#buffer.length - this.#length) {
      this.#enlarge();
    }
    this.#buffer.set(data, this.#length);
    this.#length += data.length;
    return this;
  }

  /** @return {Uint8Array} */
  get buffer() {
    return this.#buffer.subarray(0, this.#length);
  }

  get length() {
    return this.#length;
  }

  /**
   * @param {number} start
   * @param {number} [end]
   * @return {Uint8Array}
   */
  subarray(start, end) {
    return this.#buffer.subarray(start, end ?? this.#length);
  }

  /** @return {number} */
  reserved() {
    return this.#buffer.length;
  }

  /** @param {number} size */
  resize(size) {
    while (size > this.#buffer.length) {
      this.#enlarge();
    }
    this.#length = size;
    return this;
  }

  /**
   * @brief remove the first elements of the buffer
   * @param {number} [n=1]
   */
  shift(n = 1) {
    if (this.#length === 0 || !Number.isInteger(n) || n < 1) { return this; }
    if (n > this.#length) { n = this.#length; }
    const remainder = this.subarray(n);
    this.reset();
    if (remainder.length > 0) { this.add(remainder); }
    return this;
  }

  /**
   * @brief copy part of a buffer at a specific offset
   * @details on the contrary to `add` this method will drop data not fitting
   * in current buffer.
   * @param  {Uint8Array|VarBuffer} target destination buffer
   * @param  {number} [targetStart=0] target start offset
   * @param  {number} [sourceStart=0] source (this) start offset
   * @param  {number} [sourceEnd] source end offset
   * @return {number} number of bytes copied
   */
  copy(target, targetStart = 0, sourceStart = 0, sourceEnd = undefined) {
    if (target instanceof VarBuffer) {
      target = target.#buffer;
    }
    if (targetStart >= target.length) {
      return 0;
    }
    const end = Math.min(sourceEnd ?? this.#length, this.#length,
      (target.length - targetStart) + sourceStart);
    const source = this.#buffer.subarray(sourceStart, end);
    target.set(source, targetStart);
    return source.length;
  }

  #enlarge() {
    var old = this.#buffer;
    this.#buffer = new Uint8Array(this.#buffer.length * 2);
    this.#buffer.set(old.subarray(0, this.#length), 0);
  }

  /**
   * @brief decode buffer as utf-8
   * @param {Uint8Array|VarBuffer} [buffer]
   * @return {string}
   */
  decode(buffer = undefined) {
    return VarBuffer.decode((buffer instanceof VarBuffer) ?
      buffer.buffer : (buffer ?? this.buffer));
  }

  /**
   * @brief decode buffer as utf-8
   * @param {Uint8Array|VarBuffer} buffer
   * @return {string}
   */
  static decode(buffer) {
    if (!decoder) {
      decoder = new TextDecoder();
    }
    return decoder.decode((buffer instanceof VarBuffer) ?
      buffer.buffer : buffer);
  }

  /**
   * @brief encode a value to VarBuffer
   * @param {string} value
   * @return {VarBuffer}
   */
  static encode(value) {
    if (!encoder) {
      encoder = new TextEncoder();
    }
    return new VarBuffer(encoder.encode(value));
  }

  toString() {
    return this.decode(this.buffer);
  }
}

export class LineBuffer extends VarBuffer {
  static CR = 0x0D;

  static LF = 0x0A;

  lineTerm = LineBuffer.LF;

  /**
   * @returns {string|null} line from buffer
   * @details returned line contains the terminating LF
   */
  takeLine() {
    const index = this.buffer.indexOf(this.lineTerm);
    if (index >= 0) {
      const ret = this.decode(this.subarray(0, index + 1)) ?? "";
      this.shift(index + 1);
      return ret;
    }
    return null;
  }

  /**
   * @return {Generator<string>}
   */
  * lines() {
    for (let line = this.takeLine(); line !== null; line = this.takeLine())
      yield line;
  }
}
