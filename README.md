# NoDash JS/Node utilities

This repository is a set of ESM packaged convenience utilities.

It aims to replace [LoDash](https://lodash.com/) for the features that were not merged in Javascript
language.

## Installation

To install this project:
```bash
echo '@cern:registry=https://gitlab.cern.ch/api/v4/groups/mro/-/packages/npm/' > .npmrc

npm install @cern/nodash
```

## Promises utilities

The `prom` namespace has been developped as a drop-in replacement for [q](https://www.npmjs.com/package/q) and [bluebird](https://www.npmjs.com/package/bluebird), providing the features that are missing on native Promise objects.

**Note:** this module has been merged from a dedicated (and now deprecated) project : [mro/common/www/prom.js](https://gitlab.cern.ch/mro/common/www/prom.js)

### Examples

```js
const { prom } = require('@cern/nodash');
const fs = require('fs');

/* just to have an asynchronuous context */
(async function() {
  // there are more elegant ways to do this, this is just for the example
  function readdir(path) {
    const deferred = prom.makeDeferred();

    fs.readdir(path, (err, ret) => {
      if (err)
        deferred.reject(err);
      else
        deferred.resolve(ret);
    });
    return deferred.promise;
  }

  /* readdir returns a promise */
  await readdir('/tmp');

  /* promise will reject after 1sec if not resolved */
  await prom.timeout(readdir('/tmp'), 1000);

  const files = await readdir('/tmp')
  // result is not modified by our 'tap'
  .then(prom.tap((ret) => console.log(ret)));

  /* wait for a moment */
  await prom.delay(100);

  await readdir('/tmp')
  .then((ret) => prom.delay(100, ret))
  .then((ret) => {
    // ret is still our result, it went through the delay
    console.log(ret);
  });
}());
```