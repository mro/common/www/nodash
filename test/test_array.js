// SPDX-License-Identifier: Zlib
// SPDX-FileCopyrightText: 2024 CERN (home.cern)
// SPDX-Created: 2024-02-07T14:58:13
// SPDX-FileContributor: Author: Sylvain Fargier <sylvain.fargier@cern.ch>

import { first, isEmpty, last } from "../src/index.js";
import { expect } from "chai";


describe("Array utils", function() {
  it("can get `last` item", function() {
    [
      { value: [], ret: undefined },
      { value: [ 1 ], ret: 1 },
      { value: [ 1, 2, 3 ], ret: 3 },
      { value: "pwet", ret: "t" },

      { value: undefined, ret: undefined },
      { value: { potato: true }, ret: undefined }
    ].forEach((test) => {
      expect(last(test.value))
      .to.be.equal(test.ret, `test failed on ${test.value}`);
    });
  });

  it("can get `first` item", function() {
    [
      { value: [], ret: undefined },
      { value: [ 1 ], ret: 1 },
      { value: [ 1, 2, 3 ], ret: 1 },
      { value: "pwet", ret: "p" },

      { value: undefined, ret: undefined },
      { value: { potato: true }, ret: undefined }
    ].forEach((test) => {
      expect(first(test.value))
      .to.be.equal(test.ret, `test failed on ${test.value}`);
    });
  });

  it("can use `isEmpty` on items", function() {
    class EmptyClass {
      #test = 42;
    }
    class NonEmptyClass extends EmptyClass {
      test = 42;
    }
    class InheritClass extends NonEmptyClass {
    }

    [
      { obj: [], return: true },
      { obj: [ 1 ], return: false },
      { obj: "", return: true },
      { obj: "1", return: false },

      { obj: {}, return: true },
      { obj: { test: 42 }, return: false },
      { obj: new EmptyClass(), return: true },
      { obj: new NonEmptyClass(), return: false },
      { obj: new InheritClass(), return: false },

      { obj: null, return: true },
      { obj: undefined, return: true },

      { obj: 12, return: true },

      { obj: new Set(), return: true },
      { obj: new Set("test"), return: false },

      { obj: new Map(), return: true },
      { obj: new Map([ [ 1, "one" ] ]), return: false }
    ].forEach((test) => {
      expect(isEmpty(test.obj)).to.equal(test.return,
        `test failed for ${JSON.stringify(test.obj)}`);
    });
  });
});
