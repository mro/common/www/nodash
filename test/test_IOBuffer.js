
// SPDX-License-Identifier: Zlib
// SPDX-FileCopyrightText: 2024 Sylvain Fargier
// SPDX-Created: 2024-03-23T18:26:20
// SPDX-FileContributor: Author: Sylvain Fargier <fargier.sylvain@gmail.com>

import { expect } from "chai";

import { IOBufferIn, IOBufferOut, VarBuffer } from "../src/index.js";

describe("IOBuffer", function() {
  /**
   * @param {boolean|undefined} littleEndian
   */
  // eslint-disable-next-line max-statements
  function check(littleEndian) {
    var buff = new Uint8Array(55);
    var bout = new IOBufferOut(buff);

    bout.writeUint8(0x77, littleEndian);
    bout.writeUint16(0x1122, littleEndian);
    bout.writeUint32(0x33445566, littleEndian);
    /* @ts-ignore on-purpose */
    bout.writeBigUint64("1234567", littleEndian);

    bout.writeInt8(-12, littleEndian);
    bout.writeInt16(-123, littleEndian);
    bout.writeInt32(-1234, littleEndian);
    /* @ts-ignore on-purpose */
    bout.writeBigInt64("-1234567", littleEndian);

    bout.writeFloat(12.44, littleEndian);
    bout.writeDouble(123.44, littleEndian);

    bout.writeString("this is a te", 13);
    expect(bout.fail).to.equal(false);

    bout.writeUint8(0);
    expect(bout.fail).to.equal(true);

    if (!(littleEndian ?? true)) {
      expect(buff).to.deep.equal(Uint8Array.from([
        0x77,
        0x11, 0x22,
        0x33, 0x44, 0x55, 0x66,
        0, 0, 0, 0, 0, 0x12, 0xd6, 0x87,
        0xF4,
        0xFF, 0x85,
        0xFF, 0xFF, 0xFB, 0x2E,
        0xff, 0xff, 0xff, 0xff, 0xff, 0xed, 0x29, 0x79,
        0x41, 0x47, 0x0a, 0x3d,
        0x40, 0x5e, 0xdc, 0x28, 0xf5, 0xc2, 0x8f, 0x5c
      ].concat(
        "this is a te".split("").map((s) => s.charCodeAt(0)), [ 0 ])
      ));
    }
    else {
      expect(buff).to.deep.equal(Uint8Array.from([
        0x77,
        0x22, 0x11,
        0x66, 0x55, 0x44, 0x33,
        0x87, 0xd6, 0x12, 0, 0, 0, 0, 0,
        0xF4,
        0x85, 0xFF,
        0x2E, 0xFB, 0xFF, 0xFF,
        0x79, 0x29, 0xed, 0xff, 0xff, 0xff, 0xff, 0xff,
        0x3d, 0x0a, 0x47, 0x41,
        0x5c, 0x8f, 0xc2, 0xf5, 0x28, 0xdc, 0x5e, 0x40
      ].concat(
        "this is a te".split("").map((s) => s.charCodeAt(0)), [ 0 ])
      ));
    }

    var bin = new IOBufferIn(buff);
    expect(bin.readUint8(littleEndian)).to.equal(0x77);
    expect(bin.readUint16(littleEndian)).to.equal(0x1122);
    expect(bin.readUint32(littleEndian)).to.equal(0x33445566);
    expect(bin.readBigUint64(littleEndian)).to.deep.equal(1234567n);

    expect(bin.readInt8(littleEndian)).to.equal(-12);
    expect(bin.readInt16(littleEndian)).to.equal(-123);
    expect(bin.readInt32(littleEndian)).to.equal(-1234);
    expect(bin.readBigInt64(littleEndian)).to.deep.equal(-1234567n);
    expect(bin.readFloat(littleEndian)).to.be.closeTo(12.44, 0.001);
    expect(bin.readDouble(littleEndian)).to.be.closeTo(123.44, 0.001);

    expect(bin.readString(13)).to.equal("this is a te");
    expect(bin.fail).to.equal(false);

    expect(bin.readUint32(littleEndian)).to.deep.equal(NaN);
    expect(bin.fail).to.equal(true);
  }

  it("can serialize little-endian data", () => check(true));
  it("can serialize big-endian data", () => check(false));
  it("defaults to little-endian", () => check(undefined));

  describe("special cases", function() {
    it("serializes a null string", function() {
      var buff = new Uint8Array(20);
      var out = new IOBufferOut(buff);

      /* @ts-ignore on-purpose */
      out.writeString(null, 2);
      /* @ts-ignore on-purpose */
      out.writeString(undefined, 2);
      out.writeString("", 2);
      expect(out.bytesLeft).to.equal(14);
      expect(out.fail).to.equal(false);
    });
  });

  it("can construct from TypedArray objects", function() {
    const out = new IOBufferOut(new Uint32Array(2));
    expect(out.bytesLeft).to.equal(8);

    out.writeUint32(1);
    out.writeUint32(2);

    const bufferIn = new IOBufferIn(
      new Uint32Array(out.buffer?.buffer ?? new Uint32Array()));
    expect(bufferIn.readUint32()).to.equal(1);
    expect(bufferIn.readUint32()).to.equal(2);
  });

  it("can construct from ArrayBuffer objects", function() {
    const out = new IOBufferOut(new Uint32Array(2).buffer);
    expect(out.bytesLeft).to.equal(8);

    out.writeUint32(1);
    out.writeUint32(2);

    const bufferIn = new IOBufferIn(out.buffer?.buffer);
    expect(bufferIn.readUint32()).to.equal(1);
    expect(bufferIn.readUint32()).to.equal(2);
  });

  it("can construct from VarBuffer objects", function() {
    const out = new IOBufferOut(new VarBuffer().add("1234"));
    expect(out.bytesLeft).to.equal(4);

    out.writeUint32(1);

    expect(out.buffer).to.be.instanceOf(VarBuffer);
    const bufferIn = new IOBufferIn(out.buffer);
    expect(bufferIn.readUint32()).to.equal(1);
  });

  it("can work on subarrays", function() {
    const out = new IOBufferOut(new Uint32Array(2).subarray(1, 2));
    expect(out.bytesLeft).to.equal(4);

    out.writeUint32(1);

    /* use the underlying complete ArrayBuffer */
    const bufferIn = new IOBufferIn(out.buffer?.buffer);
    expect(bufferIn.readUint32()).to.equal(0);
    expect(bufferIn.readUint32()).to.equal(1);
  });

  it("can add various buffer types", function() {
    const out = new IOBufferOut(new VarBuffer());
    out.add(new Uint8Array([ 0x41, 0x42 ]));
    out.add(new Uint8Array([ 0x43, 0x44 ]).buffer);
    out.add([ 0x45, 0x46 ]);

    expect((new IOBufferIn(out.buffer?.buffer)).readString(6))
    .to.equal("ABCDEF");
  });

  it("can wrap a buffer", function() {
    const out = new IOBufferOut(new VarBuffer().resize(4));
    expect(IOBufferOut.wrap(out)).to.equal(out);
    expect(IOBufferOut.wrap(out.buffer)).to.not.equal(out);
    expect(IOBufferOut.wrap(out.buffer)).to.have.property("bytesLeft", 4);

    const bufferIn = new IOBufferIn(new VarBuffer().resize(4));
    expect(IOBufferIn.wrap(bufferIn)).to.equal(bufferIn);
    expect(IOBufferIn.wrap(bufferIn.buffer)).to.not.equal(bufferIn);
    expect(IOBufferIn.wrap(bufferIn.buffer)).to.have.property("bytesLeft", 4);
  });
});
