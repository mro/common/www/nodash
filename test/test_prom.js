// @ts-check

import { expect } from "chai";
import { prom } from "../src/index.js";

describe("prom", function() {
  /** @type {(() => void)|null} */
  let rejectionHandler = null;

  afterEach(function() {
    if (rejectionHandler) {
      if (typeof process !== "undefined" && process?.removeListener) {
        process.removeListener("unhandledRejection", rejectionHandler);
      }
      else { /* browser */
        window.removeEventListener("unhandledrejection", rejectionHandler);
      }
      rejectionHandler = null;
    }
  });

  it("can create a Deferred", async function() {
    var deferred = prom.makeDeferred();
    setTimeout(() => deferred.resolve(42), 100);

    const ret = await deferred.promise;
    expect(ret).to.equal(42);

    deferred = prom.makeDeferred();
    setTimeout(() => deferred.reject(new Error("error")), 100);

    await deferred.promise
    .then(
      () => { throw new Error("should fail"); },
      (err) => expect(err).to.have.property("message", "error")
    );
  });

  it("can use timeout on Deferred", async function() {
    var deferred = prom.makeDeferred();

    await prom.timeout(deferred, 100)
    .then(
      () => { throw new Error("should fail"); },
      (err) => expect(err).to.have.property("code", "ETIMEDOUT")
    );

    deferred = prom.makeDeferred();
    prom.timeout(deferred, 100);
    setTimeout(() => deferred.resolve(42), 10);
    expect(await deferred.promise).to.equal(42);
  });

  it("can use timeout on Deferred w/o breaking promise chain",
    async function() {
      let error = null;

      const unhandledRejection = new Promise((resolve, reject) => {

        var deferred = prom.makeDeferred();

        rejectionHandler = () => resolve(true);
        if (typeof process !== "undefined" && process?.once) {
          process.once("unhandledRejection", rejectionHandler);
        }
        else { /* browser */
          window.addEventListener("unhandledrejection", rejectionHandler);
        }

        // add timeout in a new context
        const func = function() {
          return prom.timeout(deferred, 100);
        };

        func().then(
          () => { reject("should fail"); },
          (err) => { error = err; }
        );

        // wait more than the timeout
        setTimeout(() => resolve(false), 500);
      });

      expect(await unhandledRejection).to.be.equal(false);
      expect(error).to.have.property("code", "ETIMEDOUT");
    });

  it("allows to add an 'abort' function on timeout", async function() {
    var deferred = prom.makeDeferred();
    let aborted = false;

    deferred.abort = () => { aborted = true; };

    await prom.timeout(deferred, 100)
    .then(
      () => { throw new Error("should fail"); },
      (err) => expect(err).to.have.property("code", "ETIMEDOUT")
    );

    expect(aborted).to.be.equal(true);
  });

  it("can use timeout on Promise", async function() {
    var deferred = prom.makeDeferred();

    await prom.timeout(deferred.promise, 100)
    .then(
      () => { throw new Error("should fail"); },
      (err) => expect(err).to.have.property("code", "ETIMEDOUT")
    );

    deferred = prom.makeDeferred();
    prom.timeout(deferred.promise, 100);
    setTimeout(() => deferred.resolve(42), 10);
    expect(await deferred.promise).to.equal(42);
  });

  it("can use timeout on Promise w/o breaking promise chain",
    async function() {
      let error = null;

      const unhandledRejection = new Promise((resolve, reject) => {

        var deferred = prom.makeDeferred();

        rejectionHandler = () => resolve(true);
        if (typeof process !== "undefined" && process?.once) {
          process.once("unhandledRejection", rejectionHandler);
        }
        else { /* browser */
          window.addEventListener("unhandledrejection", rejectionHandler);
        }

        // add timeout in a new context
        const func = function() {
          return prom.timeout(deferred.promise, 100);
        };

        func().then(
          () => { reject("should fail"); },
          (err) => { error = err; }
        );

        // wait more than the timeout
        setTimeout(() => resolve(false), 500);
      });

      expect(await unhandledRejection).to.be.equal(false);
      expect(error).to.have.property("code", "ETIMEDOUT");
    });


  it("can create a delay Promise", async function() {
    const start = Date.now();
    expect(await prom.delay(100, 42)).to.equal(42);
    // FIXME for some reason Node seems to be rather imprecise
    expect(Date.now()).to.be.gte(start + 99);
  });

  it("can tap a Promise", async function() {
    await Promise.resolve(42)
    .then(prom.tap((ret) => {
      expect(ret).to.equal(42);
      return 44;
    }))
    .then((ret) => expect(ret).to.equal(42));

    var called = false;
    await Promise.resolve(42)
    .then(prom.tap(async () => {
      await prom.delay(100);
      called = true;
    }))
    .then(() => expect(called).to.equal(true));
  });
});

