// SPDX-License-Identifier: Zlib
// SPDX-FileCopyrightText: 2024 CERN (home.cern)
// SPDX-FileContributor: Author: Sylvain Fargier <sylvain.fargier@cern.ch>

import { expect } from "chai";
import { LineBuffer } from "../src/index.js";

describe("LineBuffer", function() {
  it("can parse lines", function() {
    const buffer = new LineBuffer();
    buffer.add("same");
    buffer.add(" line\n\n");
    buffer.add("another\n");

    expect(buffer.takeLine()?.trim()).to.deep.equal("same line");
    expect(buffer.takeLine()?.trim()).to.deep.equal("");
    expect(buffer.takeLine()?.trim()).to.deep.equal("another");
  });

  it("can iterate on lines", function() {
    const buffer = new LineBuffer();
    buffer.add("some content\n\nsplit accross\nseveral lines");

    const ret = [];
    for (const line of buffer.lines()) {
      ret.push(line.trim());
    }
    expect(ret).to.deep.equal(
      [ "some content", "", "split accross" ]);

    buffer.add("\n");
    expect(buffer.takeLine()?.trim()).to.deep.equal("several lines");
  });
});
