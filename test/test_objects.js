// @ts-check
// SPDX-License-Identifier: Zlib
// SPDX-FileCopyrightText: 2024 CERN (home.cern)
// SPDX-Created: 2024-02-06T09:17:30
// SPDX-FileContributor: Author: Sylvain Fargier <sylvain.fargier@cern.ch>

import { cloneDeep, merge, set } from "../src/index.js";
import { expect } from "chai";


describe("Object utils", function() {
  it("can `set` values", function() {
    [
      { obj: {}, path: [ "test" ], value: 42, result: { test: 42 } },
      { obj: {}, path: [ "test", "toto" ], value: 42, result: { test: { toto: 42 } } },
      { obj: {}, path: [ "test", 12, "toto" ], value: 42, result: { test: { 12: { toto: 42 } } } },

      { obj: { toto: 12 }, path: [ "test" ], value: 42, result: { toto: 12, test: 42 } },
      { obj: { test: 12 }, path: [ "test" ], value: 42, result: { test: 42 } },
      /* override existing values */
      { obj: { test: 12 }, path: [ "test", "toto" ], value: 42, result: { test: { toto: 42 } } },

      { obj: [ 1, 2 ], path: [ 1 ], value: 42, result: [ 1, 42 ] },
      { obj: [ 1, 2, 3 ], path: [ 1 ], value: 42, result: [ 1, 42, 3 ] },
      /* creates holes in arrays */
      { obj: [], path: [ 1 ], value: 42, result: [ undefined, 42 ] },

      /* fails on string */
      { obj: "test", path: [ 1 ], value: "i", return: null },
      /* fails on invalid path */
      { obj: {}, path: null, value: "i", return: null },
      { obj: {}, path: "test", value: "i", result: { t: { e: { s: { t: "i" } } } } }

    ].forEach((test) => {
      const initial = cloneDeep(test.obj);

      const ret = set(test.obj, test.path, test.value);
      expect(ret).to.be.equal("return" in test ? test.return : test.obj,
        `test failed for ${JSON.stringify(initial)}`);

      if (ret) {
        expect(test.obj).to.deep.equal(test.result,
          `test failed for ${JSON.stringify(initial)}`);
      }
      else {
        expect(test.obj).to.deep.equal(initial);
      }
    });
  });

  it("can `cloneDeep`", function() {
    [
      { obj: { test: 42 } },
      { obj: { test: { toto: 42 } } },
      { obj: 12 },
      { obj: null },
      { obj: undefined }
    ].forEach((test) => {
      /** @type {any} */
      const ret = cloneDeep(test.obj);


      if (test.obj instanceof Object) {
        Object.entries(test.obj).forEach(([ key, value ]) => {
          expect(value).to.deep.equal(ret?.[key]);
          if (value instanceof Object) {
            expect(value).to.not.equal(ret?.[key]);
          }
        });
      }

      expect(ret).to.deep.equal(test.obj);
      if (test.obj instanceof Object) {
        expect(ret).to.not.equal(test.obj);
      }
    });
  });

  it("can `merge` objects", function() {
    [
      { obj: {}, sources: [ { test: 42 } ], result: { test: 42 } },
      {
        obj: { test: { toto: 44 } },
        sources: [ { test: { titi: 42 } } ],
        result: { test: { toto: 44, titi: 42 } }
      },
      {
        obj: { test: 12 },
        sources: [ { test: { titi: 42 } } ],
        result: { test: { titi: 42 } }
      },
      {
        obj: { },
        sources: [ undefined, null, 12, { test: 12 } ],
        result: { test: 12 }
      },
      {
        obj: null,
        sources: [ { test: 12 } ],
        result: { test: 12 }
      },
      {
        obj: undefined,
        sources: [ { test: 12 } ],
        result: { test: 12 }
      },
      {
        obj: "test", /* slightly different from lodash, we create a new object */
        sources: [ { test: 12 } ],
        result: { test: 12 }
      },
      {
        obj: 12, /* slightly different from lodash, we create a new object */
        sources: [ { test: 12 } ],
        result: { test: 12 }
      }
    ].forEach((test) => {
      const ret = merge(test.obj, ...test.sources);

      if (test.obj instanceof Object) {
        /* object must have been updated */
        expect(ret).to.equal(test.obj);
      }
      expect(ret).to.deep.equal(test.result);
    });
  });

  it("duplicates objects on `merge`", function() {
    const source = { test: { titi: 42 }, toto: [ 1, { titi: 12 } ] };
    const obj = merge({}, source);
    expect(obj.test).to.deep.equal(source.test);
    expect(obj.test).to.not.equal(source.test);

    expect(obj.toto).to.deep.equal(source.toto);
    expect(obj.toto).to.not.equal(source.toto);

    expect(obj.toto[1]).to.deep.equal(source.toto[1]);
    expect(obj.toto[1]).to.not.equal(source.toto[1]);
  });
});


