// SPDX-License-Identifier: Zlib
// SPDX-FileCopyrightText: 2024 CERN (home.cern)
// SPDX-FileContributor: Author: Sylvain Fargier <sylvain.fargier@cern.ch>

import { expect } from "chai";
import { VarBuffer } from "../src/index.js";

describe("VarBuffer", function() {
  it("can encode/decode strings", function() {
    const buffer = VarBuffer.encode("1A");
    expect(buffer.buffer)
    .to.deep.equal(new Uint8Array([ 0x31, 0x41 ]));

    expect(buffer.decode()).to.equal("1A");
  });

  it("can be enlarged on demand", function() {
    var buffer = new VarBuffer(10);
    expect(buffer.reserved()).to.be.equal(10);
    expect(buffer.length).to.be.equal(0);

    const data = Uint8Array.from([ 0x31 ]);
    for (let i = 0; i < 15; ++i) {
      buffer.add(data);
    }
    expect(buffer.length).to.be.equal(15);

    buffer.resize(30);
    expect(buffer.length).to.equal(30);
  });

  it("can use subarray like with Uint8Array", function() {
    var buffer = VarBuffer.encode("0123456789");
    expect(buffer.length).to.be.equal(10);

    expect(VarBuffer.decode(buffer.subarray(3, 7)))
    .to.be.deep.equal("3456");
  });

  it("can remove first n bytes", function() {
    var buffer = VarBuffer.encode("0123456789");
    expect(buffer.length).to.be.equal(10);

    buffer.shift(4);
    expect(VarBuffer.decode(buffer.buffer)).to.be.deep.equal("456789");
    expect(buffer.length).to.be.equal(6);
    expect(buffer.reserved()).to.be.equal(10);

    // negative inputs have no effect
    buffer.shift(-3);
    expect(buffer.toString()).to.be.deep.equal("456789");
    expect(buffer.length).to.be.equal(6);
    expect(buffer.reserved()).to.be.equal(10);

    // inputs greater than buffer length imply reset
    buffer.shift(buffer.length + 1);
    expect(buffer.buffer).to.be.deep.equal(new Uint8Array());
    expect(buffer.length).to.be.equal(0);
    expect(buffer.reserved()).to.be.equal(10);
  });

  it("can be partially copied at a specific offset", function() {
    var bufSource = VarBuffer.encode("0123456789");
    var varBufTarget = new VarBuffer(new Uint8Array(10).fill("-".charCodeAt(0)));
    var bufTarget = new Uint8Array(10).fill(".".charCodeAt(0));

    bufSource.copy(varBufTarget, 3, 3, 8);
    expect(varBufTarget.toString()).to.be.deep.equal("---34567--");

    /* bufTarget will overflow, "89" should be dropped */
    bufSource.copy(bufTarget, 2, 0);
    expect(bufTarget).to.be.deep.equal(VarBuffer.encode("..01234567").buffer);

    [
      { targetStart: 0, sourceStart: 2, sourceEnd: undefined,
        ret: 8, result: "23456789.." },
      { targetStart: 0, sourceStart: 0, sourceEnd: undefined,
        ret: 10, result: "0123456789" },
      { targetStart: 2, sourceStart: 0, sourceEnd: undefined,
        ret: 8, result: "..01234567" },
      { targetStart: 2, sourceStart: 2, sourceEnd: undefined,
        ret: 8, result: "..23456789" },
      { targetStart: 2, sourceStart: 11, sourceEnd: undefined,
        ret: 0, result: ".........." },
      { targetStart: 11, sourceStart: 0, sourceEnd: undefined,
        ret: 0, result: ".........." },
      { targetStart: 0, sourceStart: 0, sourceEnd: 11,
        ret: 10, result: "0123456789" }
    ].forEach((test) => {
      bufTarget.fill(".".charCodeAt(0));
      let ret = bufSource.copy(bufTarget,
        test.targetStart, test.sourceStart, test.sourceEnd);
      expect(VarBuffer.decode(bufTarget)).to.be.deep.equal(test.result);
      expect(ret).to.equal(test.ret);

      varBufTarget.buffer.fill(".".charCodeAt(0));
      ret = bufSource.copy(varBufTarget,
        test.targetStart, test.sourceStart, test.sourceEnd);
      expect(varBufTarget.toString()).to.be.deep.equal(test.result);
      expect(ret).to.equal(test.ret);
    });
  });

  if (typeof Buffer !== "undefined") {
    /* Node.js only test */

    it("works with Node.js Buffer objects", function() {


      const buffer = new VarBuffer();
      buffer.add(Buffer.from("12"));
      buffer.add(Buffer.from("34"));

      expect(buffer.toString()).to.equal("1234");
    });
  }
});

